module MMV.Convert where

-- version: 0.1.4.1

import Data.Char (chr, ord)
import Text.Printf

data Type = Floor | Round | Ceiling

--
-- Округлить до знака после запятой
--
roundTo :: (RealFrac a) => Int -> a -> a
roundTo n x =
    let f = round (x * y)
        y = 10 ^ fromIntegral n
        z = round x
    in
    if n == 0 then fromIntegral z else fromIntegral f / y

-- String

stringToInt :: String -> Int
stringToInt s = case reads s :: [(Int, String)] of
    [(x, "")] -> x
    _         -> 0

stringToInteger :: String -> Integer
stringToInteger s = case reads s :: [(Integer, String)] of
    [(x, "")] -> x
    _         -> 0

stringToDouble :: String -> Double
stringToDouble s = case reads s :: [(Double, String)] of
    [(x, "")] -> x
    _         -> 0

stringToFloat :: String -> Float
stringToFloat s = case reads s :: [(Float, String)] of
    [(x, "")] -> x
    _         -> 0

stringToBool :: String -> Bool
stringToBool s = not (null s)

stringToChar :: String -> Char
stringToChar s = if null s then chr 0 else head s

-- Int

intToInteger :: Int -> Integer
intToInteger = toInteger

intToFloat :: Int -> Float
intToFloat = fromIntegral

intToDouble :: Int -> Double
intToDouble = fromIntegral

intToString :: Int -> String
intToString = show

intToBool :: Int -> Bool
intToBool x = x /= 0

intToChar :: Int -> Char
intToChar x = if x < 0 || x > 255 then chr 0 else chr x

-- Integer

integerToInt :: Integer -> Int
integerToInt = fromIntegral

integerToFloat :: Integer -> Float
integerToFloat = fromIntegral

integerToDouble :: Integer -> Double
integerToDouble = fromIntegral

integerToString :: Integer -> String
integerToString = show

integerToBool :: Integer -> Bool
integerToBool x = x /= 0

integerToChar :: Integer -> Char
integerToChar x = if x < 0 || x > 255 then chr 0 else chr $ integerToInt x

-- Float

floatToInt :: Type -> Float -> Int
floatToInt Floor   = floor
floatToInt Round   = round
floatToInt Ceiling = ceiling

floatToInteger :: Type -> Float -> Integer
floatToInteger Floor   = floor
floatToInteger Round   = round
floatToInteger Ceiling = ceiling

floatToDouble :: Float -> Double
floatToDouble = realToFrac

floatToBool :: Float -> Bool
floatToBool x = x /= 0

floatToString :: Float -> String
floatToString = printf "%f"

floatToChar :: Float -> Char
floatToChar x = if x < 0 || x >= 256 then chr 0 else chr $ floatToInt Floor x

-- Double

doubleToInt :: Type -> Double -> Int
doubleToInt Floor   = floor
doubleToInt Round   = round
doubleToInt Ceiling = ceiling

doubleToInteger :: Type -> Double -> Integer
doubleToInteger Floor   = floor
doubleToInteger Round   = round
doubleToInteger Ceiling = ceiling

doubleToFloat :: Double -> Float
doubleToFloat = realToFrac

doubleToBool :: Double -> Bool
doubleToBool x = x /= 0

doubleToString :: Double -> String
doubleToString = printf "%f"

doubleToChar :: Double -> Char
doubleToChar x = if x < 0 || x >= 256 then chr 0 else chr $ doubleToInt Floor x

-- Bool

boolToInt :: Bool -> Int
boolToInt x = if x then 1 else 0

boolToInteger :: Bool -> Integer
boolToInteger x = if x then 1 else 0

boolToFloat :: Bool -> Float
boolToFloat x = if x then 1 else 0

boolToDouble :: Bool -> Double
boolToDouble x = if x then 1 else 0

boolToString :: Bool -> String
boolToString x = if x then "1" else "0"

boolToChar :: Bool -> Char
boolToChar x = if x then '1' else '0'

-- Char

charToInt :: Char -> Int
charToInt = ord

charToInteger :: Char -> Integer
charToInteger = intToInteger . ord

charToFloat :: Char -> Float
charToFloat = intToFloat . ord

charToDouble :: Char -> Double
charToDouble = intToDouble . ord

charToString :: Char -> String
charToString x = [x]

charToBool :: Char -> Bool
charToBool x = x /= '0'
