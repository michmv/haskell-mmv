
-- version: 0.1.4.0

module MMV.List
    ( listHas
    , listCount
    , listGet
    , listDel
    , listSet
    , listAddAfter
    , listAddBefore
    )
where

--
-- Проверить есть ли элемент в списке отвечающий 
-- каким то условиям, которое записано в функции
--
listHas :: [a] -> (a -> Bool) -> Bool
listHas [] _ = False
listHas (x:xs) f = f x || listHas xs f

--
-- Подсчитать количество элементов в списке отвечающие
-- какому то условию записанному в функуию
--
listCount :: [a] -> (a -> Bool) -> Int
listCount [] _ = 0
listCount (x:xs) f = if f x then 1 + listCount xs f else listCount xs f

--
-- Получить список элементов из списка, отвечающих какому то
-- условию, которое записано в функции
--
listGet :: [a] -> (a -> Bool) -> [a]
listGet [] _ = []
listGet (x:xs) f = if f x then x : listGet xs f else listGet xs f

--
-- Удалить элемент из списка отвечающему какому то условию,
-- условие записано в функции
--
listDel :: [a] -> (a -> Bool) -> [a]
listDel [] _ = []
listDel (x:xs) f = if f x then listDel xs f else x : listDel xs f

--
-- Перезаписать элемент в списке отвечающий какому то условию,
-- условие записано в функции. Если элемент не найден то никаких 
-- изменений в списке не будет
-- 
listSet :: [a] -> (a -> Bool) -> a -> [a]
listSet [] _ _ = []
listSet (x:xs) f replace =
    if f x then replace : listSet xs f replace
           else x : listSet xs f replace

--
-- Вставить элемент после элемента отвечающему какому то условию,
-- условие задается в функции
-- Если элемент не найден то никаких изменений не будет
--
listAddAfter :: [a] -> (a -> Bool) -> a -> [a]
listAddAfter [] _ _ = []
listAddAfter (x:xs) f add =
    if f x then x : add : listAddAfter xs f add
           else x : listAddAfter xs f add

--
-- Вставить элемент перед элемента отвечающему какому то условию,
-- условие задается в функции
-- Если элемент не найден то никаких изменений не будет
--
listAddBefore :: [a] -> (a -> Bool) -> a -> [a]
listAddBefore [] _ _ = []
listAddBefore (x:xs) f add =
    if f x then add : x : listAddBefore xs f add
           else x : listAddBefore xs f add
