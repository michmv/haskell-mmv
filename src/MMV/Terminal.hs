{-# LANGUAGE MultiWayIf #-}

-- version: 0.1.5.0

module MMV.Terminal
    ( Args(..)
    , MMV.Terminal.getArgs
    , readArgs
    , checkArgsByName
    , checkArgsByList
    , getArgsByName
    , getArgsByList
    , getArgsByNameExtend
    , getArgsByListExtend
    , defVal
    )
where

import MMV.List (listHas, listGet)
import MMV.String (subStr, strPos)
import Data.List ((!!))
import System.Environment as E (getArgs)

data Args = Args String -- name
                 String -- value
                 deriving (Show, Eq)

--
-- Получить список пар значений из командной строки в виде списка [Args]
--
getArgs :: IO [Args]
getArgs = E.getArgs >>= return . readArgs

--
-- Конвертировать список параметров командной строки читаемую функцией 
-- System.Environment.getArgs в [Args]
--
readArgs :: [String] -> [Args]
readArgs str = readArgsLoop_ str 1

--
-- Проверить есть ли в списке аргумент с определенным именем
--
-- @ Список аргуметнов
-- @ Имя
--
checkArgsByName :: [Args] -> String -> Bool
checkArgsByName args name = listHas args (\(Args x _) -> x == name)

--
-- Проверить если ли в списке аргумент с именем из второго списка
--
-- @ Список аргументов
-- @ Список имен для поиска
--
checkArgsByList :: [Args] -> [String] -> Bool
checkArgsByList args list = listHas args (\(Args x _) -> listHas list (== x))

--
-- Найти аргумент по имени и получить значение
--
-- @ Список аргуметнов
-- @ Имя
--
getArgsByName :: [Args] -> String -> String
getArgsByName [] _ = ""
getArgsByName args search =
    let res = listGet args (\(Args x _) -> x == search)
        f (Args _ v) = v
        l = length res
    in
    if | null res  -> ""
       | l == 1    -> f $ head res
       | otherwise -> f $ res !! (l-1)

--
-- Найти аргумент по имени из списка имен и получить значение
-- Функция вернет не пустое значение первого найденого имени из списка
--
-- @ Список аргументов
-- @ Список имен для поиска
--
getArgsByList :: [Args] -> [String] -> String
getArgsByList _ [] = ""
getArgsByList [] _ = ""
getArgsByList args (x:xs) =
    if null s then getArgsByList args xs else s
    where
        s = getArgsByName args x

--
-- Найти аргумент по имени и получить значение
-- Если у аргумента нет значения то проверяется следующий,
-- если следующий не имеет имя то берется его значение
--
-- @ Список аргуметнов
-- @ Имя
--
getArgsByNameExtend :: [Args] -> String -> String
getArgsByNameExtend [] _ = ""
getArgsByNameExtend (Args name value:xs) search =
    if name == search then readValue value (readNextElement xs)
                  else getArgsByNameExtend xs search
    where
        readNextElement :: [Args] -> Args
        readNextElement list = if null list then Args "" "" else head list

        readValue :: String -> Args -> String
        readValue value (Args name next) =
            let symbol = subStr name 5 1 in
            if value == "" && 
               (subStr name 0 5 == "index") && 
               strPos symbol "123456789" /= 0 then next
            else value

--
-- Найти аргумент по имени из списка и получить значение
-- Если у аргумента нет значения то проверяется следующий,
-- если следующий не имеет имя то берется его значение
-- Функция вернет не пустое значение первого найденого имени из списка
--
-- @ Список аргуметнов
-- @ Список имен для поиска
--
getArgsByListExtend :: [Args] -> [String] -> String
getArgsByListExtend [] _ = ""
getArgsByListExtend _ [] = ""
getArgsByListExtend args (x:xs) =
    if null s then getArgsByListExtend args xs else s
    where
        s = getArgsByNameExtend args x

--
-- Получить строку, если она пуста то получить значение по умолчанию
--
-- @ Строка
-- @ Значение по умолчанию
--
defVal :: String -> String -> String
defVal val def = if null val then def else val

-------------------------------------------------------------------------------

readArgsLoop_ :: [String] -> Int -> [Args]
readArgsLoop_ [] _ = []
readArgsLoop_ (x:xs) index =
    if haveName_ x -- check has name or not
        then
            if isFlags_ x -- check arg is flag
                then getFlagPairKeyValue_ x : readArgsLoop_ xs index
                else var : readArgsLoop_ xs index
        else
            Args ("index" ++ show index) x : readArgsLoop_ xs (index+1) -- not have name
    where
        var = getPairKeyValue_ x

--
-- Проверить есть ли у аргумента имя или он является безименнованным параметром
-- (Пример аргументов с именем -f или --test). Вернет True если указано имя или False
--
haveName_ :: String -> Bool
haveName_ s =
    head s == '-' && s /= "-" && s /= "--" && s /= "---" &&
    ( (s!!1 /= '-' && (length s == 2 || length s >=3 && s!!2 == '=')) || -- flag
      (s!!1 == '-' && s!!2 /= '-') -- var
    )

--
-- Проверить является ли аргумент списком флагов (-abc или -abc=1). Вернет True если
-- это список флагов, иначе False
--
isFlags_ :: String -> Bool
isFlags_ s = (s!!1) /= '-'

--
-- Прочитать пару key->value из аргумента
--
getPairKeyValue_ :: String -> Args
getPairKeyValue_ s =
    if searchCharEqual /= 0
        then Args (subStr s 2 (searchCharEqual-2-1)) (subStr s searchCharEqual (length s))
        else Args (subStr s 2 (length s)) ""
    where
        searchCharEqual = strPos "=" s

--
-- Получить список флагов и из значений из строки
--
getFlagPairKeyValue_ :: String -> Args
getFlagPairKeyValue_ s =
    if searchCharEqual /= 0
        then Args (subStr s 1 1) (subStr s 3 (length s))
        else Args (subStr s 1 1) ""
    where
        searchCharEqual = strPos "=" s
