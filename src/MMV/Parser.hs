{-# LANGUAGE MultiWayIf #-}

-- version: 0.1.2.0

module MMV.Parser
    ( escape
    , unEscape
    , explodeSmart
    , explodeSmartLimit
    , explodeSmartStringToList
    , explodeSmartQuote
    , explodeSmartQuoteLimit
    , explodeSmartQuoteStringToList
    , strPosSmart
    , strPosSmartQuote
    , checkQuotes
    , deleteQuotes
    , deleteQuotesSmart
    )
where

import MMV.String (subStr, explodeStringToList, implode, multStr, strFindSub)

--
-- Экранировать подстроку в строке, добавив перед подстрокой обратные слэши
--
-- @ Подстрока для поиска
-- @ Строк
--
escape :: String -> String -> String
escape separator str =
    implode ("\\" ++ separator) $ addBackSlashEnd $ explodeStringToList 0 0 separator str

--
-- Удалить экранирование созданное escape
--
-- @ Подстрока с помощью которой и выполнялось escape
-- @ Строка
--
unEscape :: String -> String -> String
unEscape separator str =
    let t = map f
        f x = cleanDoubleBackSlashEnd x (countBackSlashEnd x) in
    implode separator (t (explodeStringToList 0 0 separator str))

--
-- Разбить строку на список по образцу подстроки с учетом экранирования
-- Смотри explodeSmartStringToList
--
-- @ подстрока по которой надо делить
-- @ строка
--
explodeSmart :: String -> String -> [String]
explodeSmart = explodeSmartStringToList 0 0

--
-- Разбить строку на список по образцу подстроки с учетом экранирования
-- Смотри explodeSmartStringToList
--
-- @ Количество элементов в итоговом списке, 0 если не надо ограничивать
-- @ Подстрока по которой надо делить
-- @ Cтрока
--
explodeSmartLimit :: Int -> String -> String -> [String]
explodeSmartLimit = explodeSmartStringToList 0

--
-- Прочитать строку в список разбив по подстроке с учетом экранирования
--
-- text++text     -> [text, text]
-- text\++text    -> [text++text]
-- text\\++text   -> [text\, text]
-- text\\\++text  -> [text\++text]
-- text\\\\++text -> [text\\, text]
--
-- @ Указывает от куда начать поиск разделителя, начало строки соответствует 0
-- @ Указывает максимальное количество строк в списке, если указать 0 то строка
--     будет полностью разбита по разделителю, если значение больше нуля то в последней 
--     элементе в списке будет остаток строки
-- @ Разделитель для строки
-- @ Строка
--
explodeSmartStringToList :: Int -> Int -> String -> String -> [String]
explodeSmartStringToList  begin count separator str =
    mergeBackAfterExplode separator (explodeStringToList begin count separator str)

--
-- Разбить строку на элементы списка группируя символы с помощью кавычек, экранирование
-- учитывается
-- Смотри explodeSmartQuoteStringToList
--
explodeSmartQuote :: String -> [String]
explodeSmartQuote = explodeSmartQuoteStringToList 0 0

--
-- Разбить строку на элементы списка, с указанием лимита для списка, группируя символы 
-- с помощью кавычек, экранирование учитывается
-- Смотри explodeSmartQuoteStringToList
--
explodeSmartQuoteLimit :: Int -> String -> [String]
explodeSmartQuoteLimit = explodeSmartQuoteStringToList 0

--
-- Разбить строку на элементы списка групируя символы с помощью кавычек, экранирование
-- учитывается
--
-- 0 -> 0 -> "a"-'b'-`c` -> ["a", -, 'b', -, `c`]
-- 0 -> 4 -> "a"-'b'-`c` -> ["a", -, 'b', -`c`]
-- 0 -> 3 -> "a"-'b'-`c` -> ["a", -, 'b'-`c`]
-- 0 -> 2 -> "a"-'b'-`c` -> ["a", -'b'-`c`]
-- 0 -> 1 -> "a"-'b'-`c` -> ["a"-'b'-`c`]
--
-- ">\"<"'>\'<'`>\`<` -> [">"<", '>'<', `>`<`]
--
-- @ Начало разделение строки, 0 это начало строки
-- @ Лимит элементов в списку, если указать 0 то будут получены все элементы
-- @ Строка
--
explodeSmartQuoteStringToList :: Int -> Int -> String -> [String]
explodeSmartQuoteStringToList n count str =
    let l = subStr str n 1 in
    if | n >= length str || count == 1 -> if null str then [] else [str]
       | l == "'"  -> getQuoteIn n "'" str
       | l == "`"  -> getQuoteIn n "`" str
       | l == "\"" -> getQuoteIn n "\"" str
       | otherwise -> explodeSmartQuoteStringToList (n+1) count str
    where
        getQuoteIn :: Int -> String -> String -> [String]
        getQuoteIn n symbol str =
            let t = strPosSmart symbol (drop (n+1) str)
                beforeQuote = take n str
                quote = unEscape symbol (take (t+1) (drop n str))
                afterQuote = drop (n+t+1) str
            in
            if t > 0
                then if n == 0
                    then quote : explodeSmartQuoteStringToList 0 (count-1) afterQuote
                    else if count <= 2
                        then beforeQuote : explodeSmartQuoteStringToList 0 (count-1) (drop n str)
                        else beforeQuote : quote : explodeSmartQuoteStringToList 0 (count-2) (drop (n+t+1) str)
                else explodeSmartQuoteStringToList (n+1) count str

--
-- Найти первой вхождение подстроки в строку с учетом экранирования через обратный слэш
-- Вернет номер позиции или 0 если ничего не будте найдено
-- Первый символ строки начинается с 1
--
-- @ Подстрока для поиска
-- @ Строка
--
strPosSmart :: String -> String -> Int
strPosSmart = strPos_ 0 ""
    where
        strPos_ :: Int -> String -> String -> String -> Int
        strPos_ n buffer search str =
            if | n >= length str -> 0
               | subStr str n (length search) == search ->
                    if odd (length buffer)
                        then strPos_ (n+1) "" search str
                        else n+1
               | subStr str n 1 == "\\" -> strPos_ (n+1) ('\\':buffer) search str
               | otherwise -> strPos_ (n+1) "" search str

--
-- Найти позицию подстроки в строке пропуская текст в кавычках ("", '', ``)
-- Вернет 0 если подстрока не найдена
-- Первый символ строки начинается с 1
--
-- @ Подстрока
-- @ Строка
--
strPosSmartQuote :: String -> String -> Int
strPosSmartQuote = strPosSmartQuote_ 0
    where
        strPosSmartQuote_ :: Int -> String -> String -> Int
        strPosSmartQuote_ n search str =
            let next x = strPosSmartQuote_ (n + strPosSmart x (drop (n+1) str) + 1) search str
                l = subStr str n 1
            in
            if | n >= length str -> 0
               | subStr str n (length search) == search -> n+1
               | l == "`" -> next "`"
               | l == "'" -> next "'"
               | l == "\"" -> next "\""
               | otherwise -> strPosSmartQuote_ (n+1) search str

--
-- Проверить заключена ли строка в кавычки, кавычки стоят первым и последним символом
--
checkQuotes :: String -> Bool
checkQuotes str =
    let s = subStr str 0 1
        e = subStr str (-1) 1
    in
    s == e && strFindSub s "`'\""

--
-- Удалить кавычки вокруг строки
--
deleteQuotes :: String -> String
deleteQuotes str = if checkQuotes str then subStr str 1 (-1) else str

--
-- Удалить кавычки вокруг строки и удаляет экранирование внутри этой строки
--
deleteQuotesSmart :: String -> String
deleteQuotesSmart str =
    let c = subStr str 0 1 in
    if checkQuotes str then unEscape c (subStr str 1 (-1)) else str

--------------------------------------------------------------------------------

--
-- Пробежать по списку и у каждой строки найти обратные слэши на конце и увеличить их
-- количество вдвое
--
addBackSlashEnd :: [String] -> [String]
addBackSlashEnd = map (\x -> x ++ multStr (countBackSlashEnd x) "\\")

--
-- Подсчитать количество обратный слэшей на конце
--
countBackSlashEnd :: String -> Int
countBackSlashEnd str = calc (reverse str)
    where
        calc :: String -> Int
        calc [] = 0
        calc (x:xs) = if x == '\\' then 1 + calc xs else 0

--
-- Удалить половину обратный слэши на конце строки, если их нечетное количество то
-- лишний слэш тоже удаляется
--
-- @ Строка
-- @ Количество обратных слэшей на конце (количество обратных слешей уж подсчитано)
--
cleanDoubleBackSlashEnd :: String -> Int -> String
cleanDoubleBackSlashEnd str 0 = str
cleanDoubleBackSlashEnd str count = subStr str 0 ((count `quot` 2 + count `rem` 2) * (-1))

--
-- Пройтись по списку разделенного фунцией explode и объединить все элементы которые
-- имеют но конце символ экранирования т.е. для них не нужно было выполнять разделение.
-- Из конца строки удаляются все символы экранирования обратного слэша
--
--                         IN               OUT
-- text-\|-text  -> [text-\, -text]  -> [text-|-text]
-- text-\\|-text -> [text-\\, -text] -> [text-\, -text]
--
-- @ Подстрока с помощью которой делалось разбивка
-- @ Список строк
--
mergeBackAfterExplode :: String -> [String] -> [String]
mergeBackAfterExplode _ [] = []
mergeBackAfterExplode separator (x:y:t) =
    let n = countBackSlashEnd x in
        if odd n then 
            mergeBackAfterExplode separator ((cleanDoubleBackSlashEnd x n ++ separator ++ y) : t)
        else cleanDoubleBackSlashEnd x n : mergeBackAfterExplode separator (y:t)
mergeBackAfterExplode _ (x:y) = [x]
