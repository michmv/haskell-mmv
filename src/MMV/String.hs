{-# LANGUAGE MultiWayIf #-}

-- version: 0.1.6.0

module MMV.String (
        lTrim,
        lTrimCustom,
        rTrim,
        trim,
        explodeSegments,
        explode,
        explodeLimit,
        explodeStringToList,
        implode,
        strPos,
        strFindSub,
        strCountSub,
        subStr,
        strReplace,
        multStr
    ) where

--
-- Удалить пробельные символы из начала строки
--
lTrim :: String -> String
lTrim = lTrimCustom " \t\n\r\0\v\f"

--
-- Удалить пробельные символы из конца строки
--
rTrim :: String -> String
rTrim str = reverse (lTrimCustom " \t\n\r\0\v\f" (reverse str))

--
-- Удалить пробельные символы из начала и конца строки
--
trim :: String -> String
trim = lTrim . rTrim

--
-- Удалить произвольный набор символов из начала строки
--
-- @ символов для удаления
-- @ строка
--
lTrimCustom :: String -> String -> String
lTrimCustom _ [] = ""
lTrimCustom symbols (x:xs) = if findCharInString x symbols
    then lTrimCustom symbols xs
    else x : xs

--
-- Разбить строку на отрезки определенной длины
--
-- @ длина отрезка
-- @ строка
--
explodeSegments :: Int -> String -> [String]
explodeSegments n str =
    if null str || n == 0 then []
    else
        let next = drop n str in
        subStr str 0 n : explodeSegments n next

--
-- Разбить строку на список по образцу подстроки
--
-- @ подстрока по которой надо делить
-- @ строка
--
explode :: String -> String -> [String]
explode = explodeStringToList 0 0

--
-- Разбить строку на список по образцу подстроки
--
-- @ Количество элементов в итоговом списке, 0 если не надо ограничивать
-- @ Подстрока по которой надо делить
-- @ Cтрока
--
explodeLimit :: Int -> String -> String -> [String]
explodeLimit = explodeStringToList 0

--
-- Разобрать строку в список с помощью подстроки
--
-- @ указывает от куда начать поиск разделителя, начало строки соответствует 0
-- @ указывает максимальное количество строк в списке, если указать 0 то строка
--   будет полностью разбита по разделителю, если значение больше нуля то в последней
--   строке в списке будет остаток строки
-- @ разделитель для строки
-- @ строка
--
explodeStringToList :: Int -> Int -> String -> String -> [String]
explodeStringToList begin count separator str =
    if | begin+1 > length str || count == 1 ->
            [str]
       | separator == subStr str begin (length separator) ->
            subStr str 0 begin : explodeStringToList 0 (count-1) separator (drop (begin+length separator) str)
       | otherwise ->
            explodeStringToList (begin+1) count separator str

--
-- Собрать из списка строку используя подстроку для соединениями
--
-- @ Подстрока
-- @ Список
--
implode :: String -> [String] -> String
implode _ [] = ""
implode separator (x:xs) = x ++ implode_loop separator xs
    where
        implode_loop _ [] = ""
        implode_loop separator (x:xs) = separator ++ x ++ implode_loop separator xs

--
-- Получить подстроку
--
-- @ Строка
-- @ Начало подстроки, 0 указывает на самое начало исходной строки
-- @ Длинна подстроки
--
subStr :: String -> Int -> Int -> String
subStr str start length_sub = take y (drop x str)
    where
        x = if start >= 0 then start else max+start
        y = if length_sub >= 0 then length_sub else max-x+length_sub
        max = length str

--
-- Найти номер первого вхождения подстроки в строке. Отчет начинается с 1
-- Если подстрока не найдена то вернет 0
--
-- @ Подстрока для поиска
-- @ Строка
--
strPos :: String -> String -> Int
strPos search str =
    let searchSubString = \num search str ->
            if num+l-1 > max then 0
            else
                if search == subStr str (num-1) l then num
                else searchSubString (num+1) search str
            where
                l = length search
                max = length str
    in
    searchSubString 1 search str

--
-- Найти количество вхождений подстроки в строку
--
-- @ Подстрока для поиска
-- @ Строка
--
strCountSub :: String -> String -> Int
strCountSub search str = strCountSub_ search str 0
    where
        strCountSub_ :: String -> String -> Int -> Int
        strCountSub_ search str n = let x = strPos search str in
            if x == 0 then n
            else strCountSub_ search (drop (x - 1 + length search) str) $! (n+1)

--
-- Проверить входит ли подстрока в строку
--
-- @ Подстрока для поиска
-- @ Строка
--
strFindSub :: String -> String -> Bool
strFindSub search str = strPos search str /= 0

--
-- Сделать замену в строке
--
-- @ Строка для поиска
-- @ Строка для замены
-- @ Строка
--
strReplace :: String -> String -> String -> String
strReplace _ _ [] = ""
strReplace search replace str =
    let l = length search in
    if subStr str 0 l == search
        then replace ++ strReplace search replace (drop l str)
        else head str : strReplace search replace (tail str)

--
-- Создать строку из повторяющихся подстрок
--
-- @ Количество повторений
-- @ Подстрока
--
multStr :: Int -> String -> String
multStr n str = if n <= 0 then "" else str ++ multStr (n-1) str

--------------------------------------------------------------------------------

--
-- Поиск символа в строке
--
findCharInString :: Char -> String -> Bool
findCharInString _ [] = False
findCharInString x (s:ss) = (s == x) || findCharInString x ss
