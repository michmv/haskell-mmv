{-# LANGUAGE MultiWayIf #-}

-- version: 0.1.2.0

module MMV.Config
    ( readConfigToList
    , readConfigToListMult
    , writeListToConfig
    , writeListToConfigMult
    , hasConfigByName
    , getConfigByName
    , setConfigByName
    , delConfigByName
    , asInt, asInteger, asFloat, asDouble
    )
where

import MMV.String
    ( explode
    , explodeLimit
    , trim
    , lTrim
    , subStr
    , strFindSub
    , implode
    )

import MMV.Parser
    ( strPosSmartQuote
    , explodeSmartLimit
    , unEscape
    , explodeSmartQuote
    , explodeSmartQuoteLimit
    , explodeSmart
    , escape
    , checkQuotes
    , deleteQuotesSmart
    )

import MMV.Convert
    (stringToInt, stringToInteger, stringToFloat, stringToDouble)

--
-- Чтение строк из конфига:
--     # comments
--     name1 value1
--     name2 "text with space"
--     name4 "text with \"quotes\"" # comments
--     name4 value1 value2
--  В лист вида:
--      [("name1", "value1"), ("name2", "value2")]
-- 
readConfigToList :: String -> [(String, String)]
readConfigToList str =
    let list = cleanComment_ . cleanEmpty_ $ explode "\n" str in
    map (createPairSingle_ . trim . cleanCommentFromString_) list

--
-- Чтение строк из конфига:
--     # comments
--     name1 value1
--     name2 "text with space"
--     name4 "text with \"quotes\"" # comments
--     name4 value1 value2
--  В лист вида:
--      [("name1", ["values1"]), ("name2", ["values2"])]
-- 
-- Отличие от readConfigToList в том что одно имя может иметь несколько
-- значений разделенных пробелами
-- 
readConfigToListMult :: String -> [(String, [String])]
readConfigToListMult str =
    let list = cleanComment_ . cleanEmpty_ $ explode "\n" str in
    map (createPairMultiple_ . trim . cleanCommentFromString_) list

--
-- Конвертировать список пар в строковое представление для последующей
-- записи в конфиг что бы это можно было читать readConfigToList
--
writeListToConfig :: [(String, String)] -> String
writeListToConfig [] = ""
writeListToConfig (x:xs) =
    prepereString_ (fst x) ++ " " ++ prepereString_ (snd x) ++ "\n" ++ writeListToConfig xs

--
-- Конвертировать список пар в строковое представление для последующей
-- записи в конфиг что бы это можно было читать readConfigToListMult
-- Отличие от writeListToConfig состоит в том что одному имени может
-- соответствовать несколько значений
--
writeListToConfigMult :: [(String, [String])] -> String
writeListToConfigMult [] = ""
writeListToConfigMult (x:xs) =
    let func a b = prepereString_ a ++ " " ++ b in
    prepereString_ (fst x) ++ " " ++ foldr func "" (snd x) ++ "\n" ++ writeListToConfigMult xs

--
-- Проверить есть ли в конфиге переменная
--
hasConfigByName :: [(String, a)] -> String -> Bool
hasConfigByName [] _ = False
hasConfigByName (x:xs) name = (fst x == name) || hasConfigByName xs name

--
-- Извлечь значение из конфига
--
getConfigByName :: [(String, a)] -> String -> Maybe a
getConfigByName [] _ = Nothing
getConfigByName (x:xs) name = if fst x == name then Just (snd x)
                                               else getConfigByName xs name

--
-- Установить значение у переменной, если переменной нет то создается новая
-- Если в конфиге несколько переменных с таким именем то значение получают все
--
setConfigByName :: [(String, a)] -> String -> a -> [(String, a)]
setConfigByName config name value = setConfigByName' config name value False
    where
        setConfigByName' :: [(String, a)] -> String -> a -> Bool -> [(String, a)]
        setConfigByName' [] name value flag = if flag then [] else [(name, value)]
        setConfigByName' (x:xs) name value flag =
            if fst x == name then (name, value) : setConfigByName' xs name value True
                             else x : setConfigByName' xs name value flag

--
-- Удалить переменную из конфига
--
delConfigByName :: [(String, a)] -> String -> [(String, a)]
delConfigByName [] _ = []
delConfigByName (x:xs) name = if fst x == name then delConfigByName xs name
                                               else x : delConfigByName xs name

--
-- Из строки в Int
--
asInt :: String -> Int
asInt = stringToInt

--
-- Из строки в Integer
--
asInteger :: String -> Integer
asInteger = stringToInteger

--
-- Из строки в Float
--
asFloat :: String -> Float
asFloat = stringToFloat

--
-- Из строки в Double
--
asDouble :: String -> Double
asDouble = stringToDouble

----------------------------------------------------------------------

--
-- Удалить из строки комментарий начинающийся с #, с учетом кавычек
--
cleanCommentFromString_ :: String -> String
cleanCommentFromString_ str =
    let t = strPosSmartQuote "#" str in
    if t > 0 then take (t-1) str else str

--
-- Очистить список от строк начинающихся с комментов
--
cleanComment_ :: [String] -> [String]
cleanComment_ [] = []
cleanComment_ (s:ss) = if checkIsCommentLine_ s
        then cleanComment_ ss
        else s : cleanComment_ ss
    where
        checkIsCommentLine_ :: String -> Bool
        checkIsCommentLine_ [] = False
        checkIsCommentLine_ (x:xs) =
            x == '#' || ((x == ' ' || x == '\t' || x == '\r') && checkIsCommentLine_ xs)

--
-- Очистить список от пустых строк
--
cleanEmpty_ :: [String] -> [String]
cleanEmpty_ [] = []
cleanEmpty_ (l:ls) = if checkEmptyLine_ l then cleanEmpty_ ls else l : cleanEmpty_ ls
    where
        checkEmptyLine_ str = null (lTrim str)

--
-- Удалить кавычки ("'`) обрамляющие строку и снять экранирование с них если есть такие внутри
--
cleanQuotes_ :: String -> String
cleanQuotes_ str =
    let s = subStr str 0 1
        e = subStr str (-1) 1
    in
    if s == e && strFindSub s "`'\"" then unEscape s (subStr str 1 (-1))
                                     else str

--
-- Разбить строку на список пар, первый содержит имя, второй значение
--
createPairSingle_ :: String -> (String, String)
createPairSingle_ str =
    let s1 = explodeSmartQuoteLimit 2 str
        h1 = head s1
        tailStr s = if length s < 1 then "" else head s
        s2 = explodeLimit 2 " " h1
        h2 = head s2
        t2 = implode " " (tail s1) ++ (lTrim . tailStr $ tail s2)
    in
    if checkQuotes h1 then (deleteQuotesSmart h1, deleteQuotesSmart (lTrim . tailStr $ tail s1))
                      else (h2, deleteQuotesSmart t2)

--
-- Разбить строку на список пар, первый содержит имя, второй список значений
--
createPairMultiple_ :: String -> (String, [String])
createPairMultiple_ str =
    let s = cleanEmpty_ . explodeValues . explodeSmartQuote $ str
        l = length s
    in
    if | l == 0 -> ("",[])
       | l == 1 -> (head s, [])
       | otherwise -> (head s, tail s)
    where
        explodeValues :: [String] -> [String]
        explodeValues [] = []
        explodeValues (x:xs) =
            let s = subStr x 0 1
                e = subStr x (-1) 1
            in
            if s == e && strFindSub s "`'\"" then unEscape s (subStr x 1 (-1)) : explodeValues xs
                                             else explodeSmart " " x ++ explodeValues xs

--
-- Добавить ковычки, если они нужны
--
prepereString_ :: String -> String
prepereString_ str = if checkSpace str || checkQuotes str
    then "\"" ++ escape "\"" str ++ "\""
    else str
    where
        checkSpace = strFindSub " "
        checkQuotes x = strFindSub "\"" x || strFindSub "'" x || strFindSub "`" x
