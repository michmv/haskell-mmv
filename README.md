# MMV

Use:
```
build-depends: mmv
```

Lib:

- MMV.String
- MMV.List
- MMV.Parser
- MMV.Terminal
- MMV.Config
- MMV.Convert

