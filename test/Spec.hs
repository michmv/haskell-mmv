import Test.Hspec
import MMV

list = [1,2,3,4]

arguments1 = [
    "-da",
    "--name=value1",
    "-u=test",
    "--var=value2",
    "value3",
    "-f",
    "value4" ]

expected1 = [
    Args "index1" "-da",
    Args "name" "value1",
    Args "u" "test",
    Args "var" "value2",
    Args "index2" "value3",
    Args "f" "",
    Args "index3" "value4"]

config = "" ++
    "# comments" ++ "\n" ++
    "name1    value1  " ++ "\n" ++
    "  name2 \"text with space\"" ++ "\n" ++
    "name3 \"text with \\\"quotes\\\"\" # comments" ++ "\n" ++
    "name4 text text" ++ "\n" ++
    "'name5 test' value" ++ "\n" ++
    "'name6 \\'test\\'' value" ++ "\n"

expected2 = [("name1", "value1"),
             ("name2", "text with space"),
             ("name3", "text with \"quotes\""),
             ("name4", "text text"),
             ("name5 test", "value"),
             ("name6 'test'", "value")
            ]

expected3 = [("name1", ["value1"]),
             ("name2", ["text with space"]),
             ("name3", ["text with \"quotes\""]),
             ("name4", ["text", "text"]),
             ("name5 test", ["value"]),
             ("name6 'test'", ["value"])
            ]

config2 =
    [ ("name1", "value1")
    , ("name2", "value2 more")
    , ("name3", "value3 \"more\"")
    , ("name4", "\"value4\"")
    , ("name5 test", "value5")
    , ("name6 \"test\"", "value6")
    ]

config3 =
    [ ("name1", ["value1.1", "value1.2"])
    , ("name2", ["value1 more", "value2"])
    , ("name3", ["value3 \"more\""])
    , ("name4", ["\"value4\""])
    , ("name5 test", ["value5"])
    , ("name6 \"test\"", ["value6"])
    ]

main :: IO ()
main = hspec $ do

    -- MMV.String

    describe "MMV.String" $ do

        it "remove space symbol" $ trim " \t\n\r\0\vt \t\n\r\0\v" `shouldBe` "t"

        it "remove space symbol from begin string" $ lTrim " \t\n\r\0\vt" `shouldBe` "t"

        it "remove space symbol from end string" $ rTrim "t \t\n\r\0\v" `shouldBe` "t"

        -- subStr

        it "subStr 0 2" $ subStr "string" 0 2 `shouldBe` "st"

        it "subStr 0 -2" $ subStr "string" 0 (-2) `shouldBe` "stri"

        it "subStr 0 0" $ subStr "string" 0 0 `shouldBe` ""

        it "subStr 2 2" $ subStr "string" 2 2 `shouldBe` "ri"

        it "subStr -3 2" $ subStr "string" (-3) 2 `shouldBe` "in"

        it "subStr -2 -1" $ subStr "string" (-2) (-1) `shouldBe` "n"

        -- explode

        it "explodeSegments 1" $ explodeSegments 0 "123" `shouldBe` []
        it "explodeSegments 1" $ explodeSegments 2 "123" `shouldBe` ["12", "3"]
        it "explodeSegments 2" $ explodeSegments 2 "1234" `shouldBe` ["12", "34"]
        it "explodeSegments 2" $ explodeSegments 8 "1234" `shouldBe` ["1234"]

        it "explode" $ explode "++" "a++b++c++d" `shouldBe` ["a","b","c","d"]

        it "explodeLimit" $ explodeLimit 2 "++" "a++b++c++d" `shouldBe` ["a","b++c++d"]

        it "explodeStringToList" $ explodeStringToList 3 2 "++" "a++b++c++d" `shouldBe` ["a++b","c++d"]

        -- implode

        it "implode list" $ implode "++" ["a","b","c"] `shouldBe` "a++b++c"

        it "implode empty list" $ implode "++" [] `shouldBe` ""

        -- strPos

        it "find sub string" $ strPos "ty" "--ty--" `shouldBe` 3

        it "not find sub string" $ strPos "ty" "--t-y--" `shouldBe` 0

        -- strCountSub

        it "count find in string" $ strCountSub "aa" "aa aa" `shouldBe` 2

        it "count find in string 1" $ strCountSub "aa" " aa a" `shouldBe` 1

        it "count find in string 0" $ strCountSub "b" "a a" `shouldBe` 0

        -- strFindSub

        it "find sub string" $ strFindSub "ty" "--ty--" `shouldBe` True

        it "not find sub string" $ strFindSub "ty" "--t-y--" `shouldBe` False

        -- strReplace

        it "replace in string" $ strReplace "++" "ll" "he++o" `shouldBe` "hello"

        -- multStr

        it "+ -> +++" $ multStr 3 "+" `shouldBe` "+++"

    -- MMV.List

    describe "MMV.List" $ do

        it "listHas" $ listHas list (\x -> x == 3)
            `shouldBe` True

        it "listHas, not found" $ listHas list (\x -> x == 10)
            `shouldBe` False

        it "listCount" $ listCount list (\x -> x == 1 || x == 3)
            `shouldBe` 2

        it "listCount not found" $ listCount list (\x -> x == 10)
            `shouldBe` 0
    
        it "listGet" $ listGet list (\x -> x == 1 || x == 3)
            `shouldBe` [1,3]
    
        it "listGet, not found" $ listGet list (\x -> x == 10)
            `shouldBe` []

        it "listDel" $ listDel list (\x -> x == 1 || x == 3)
            `shouldBe` [2,4]

        it "listSet" $ listSet list (\x -> x == 1 || x == 3) 5
            `shouldBe` [5,2,5,4]

        it "listSet, not found" $ listSet list (\x -> x == 10) 5
            `shouldBe` [1,2,3,4]

        it "listAddAfter" $ listAddAfter list (\x -> x == 1 || x == 3) 5
            `shouldBe` [1,5,2,3,5,4]

        it "listAddAfter, not found" $ listAddAfter list (\x -> x == 10) 5
            `shouldBe` [1,2,3,4]

        it "listAddBefore" $ listAddBefore list (\x -> x == 1 || x == 3) 5
            `shouldBe` [5,1,2,5,3,4]

        it "listAddBefore, not found" $ listAddBefore list (\x -> x == 10) 5
            `shouldBe` [1,2,3,4]

    -- MMV.Parser

    describe "MMV.Parser: explodeSmart" $ do
        it "text++text     -> [text, text]" $
            explodeSmartStringToList 0 0 "++" "text++text"
                `shouldBe` ["text", "text"]

        it "text\\++text    -> [text++text]" $
            explodeSmartStringToList 0 0 "++" "text\\++text"
                `shouldBe` ["text++text"]

        it "text\\\\++text   -> [text\\, text]" $
            explodeSmartStringToList 0 0 "++" "text\\\\++text"
                `shouldBe` ["text\\", "text"]

        it "text\\\\\\++text  -> [text\\++text]" $
            explodeSmartStringToList 0 0 "++" "text\\\\\\++text"
                `shouldBe` ["text\\++text"]

        it "text\\\\\\\\++text -> [text\\\\, text]" $
            explodeSmartStringToList 0 0 "++" "text\\\\\\\\++text"
                `shouldBe` ["text\\\\", "text"]

    describe "MMV.Parser: explodeSmartQuote" $ do
        -- 0 -> 0 -> "a"-'b'-`c` -> ["a", -, 'b', -, `c`]
        it "0 -> \"a\"-'b'-`c`   -> [\"a\", -, 'b', -, `c`]" $
            explodeSmartQuoteStringToList 0 0 "\"a\"-'b'-`c`"
                `shouldBe` ["\"a\"", "-", "'b'", "-", "`c`"]

        -- 0 -> 4 -> "a"-'b'-`c` -> ["a", -, 'b', -`c`]
        it "4 -> \"a\"-'b'-`c`   -> [\"a\", -, 'b', -`c`]" $
            explodeSmartQuoteStringToList 0 4 "\"a\"-'b'-`c`"
                `shouldBe` ["\"a\"", "-", "'b'", "-`c`"]

        -- 0 -> 3 -> "a"-'b'-`c` -> ["a", -, 'b'-`c`]
        it "3 -> \"a\"-'b'-`c`   -> [\"a\", -, 'b'-`c`]" $
            explodeSmartQuoteStringToList 0 3 "\"a\"-'b'-`c`"
                `shouldBe` ["\"a\"", "-", "'b'-`c`"]

        -- 0 -> 2 -> "a"-'b'-`c` -> ["a", -'b'-`c`]
        it "2 -> \"a\"-'b'-`c`   -> [\"a\", -'b'-`c`]" $
            explodeSmartQuoteStringToList 0 2 "\"a\"-'b'-`c`"
                `shouldBe` ["\"a\"", "-'b'-`c`"]

        -- 0 -> 1 -> "a"-'b'-`c` -> ["a"-'b'-`c`]
        it "1 -> \"a\"-'b'-`c`   -> [\"a\"-'b'-`c`]" $
            explodeSmartQuoteStringToList 0 1 "\"a\"-'b'-`c`"
                `shouldBe` ["\"a\"-'b'-`c`"]

        -- ">\"<"'>\'<'`>\`<` -> [">"<", '>'<', `>`<`]
        it "\">\\\"<\"'>\\'<'`>\\`<` -> [\">\"<\", '>'<', `>`<`]" $
            explodeSmartQuoteStringToList 0 0 "\">\\\"<\"'>\\'<'`>\\`<`"
                `shouldBe` ["\">\"<\"", "'>'<'", "`>`<`"]

    describe "MMV.Parser: escape - unEscape" $ do
        it "escape without back slash" $ escape "++" "test++test"
            `shouldBe` "test\\++test"

        it "escape with back slash" $ escape "++" "test\\++test"
            `shouldBe` "test\\\\\\++test"

        it "unEscape" $ unEscape "++" "test\\++test\\\\++test\\\\\\++test\\\\\\\\++test"
            `shouldBe` "test++test\\++test\\++test\\\\++test"

        it "escape -> unEscape -> escape" $
            unEscape "++" (escape "++" "test++test\\++test\\\\++test") `shouldBe`
                "test++test\\++test\\\\++test"

    describe "MMV.Parser: strPosSmart" $ do
        it "find first not escape sub string" $ strPosSmart "++" "text\\++text++text" `shouldBe` 12

        it "not found" $ strPosSmart "++" "text\\++text" `shouldBe` 0

    describe "MMV.Parser: strPosSmartQuote" $ do
        it "find first sub string" $ strPosSmartQuote "++" "`text++text`++text" `shouldBe` 13

        it "not found" $ strPosSmartQuote "++" "`text\\`++text`" `shouldBe` 0

    describe "MMV.Parser: checkQuotes, deleteQuotes" $ do
        it "delete quotes" $ deleteQuotes "'test'" `shouldBe` "test"

        it "not delete quotes" $ deleteQuotes "test 'test'" `shouldBe` "test 'test'"

    -- MMV.Terminal

    describe "MMV.Terminal" $ do

        it "get list pairs from list arguments" $
            readArgs arguments1 `shouldBe` expected1
        
        it "check has name" $
            checkArgsByName expected1 "name" `shouldBe` True
        
        it "check not exists name" $
            checkArgsByName expected1 "error" `shouldBe` False

        it "check has name by list" $
            checkArgsByList expected1 ["z", "u"] `shouldBe` True

        it "check has name by list" $
            checkArgsByList expected1 ["error"] `shouldBe` False
        
        it "get name value" $
            getArgsByName expected1 "name" `shouldBe` "value1"

        it "get not exists name value" $
            getArgsByName expected1 "error" `shouldBe` ""

        it "get flag value" $
            getArgsByName expected1 "f" `shouldBe` ""

        it "get name from list" $
            getArgsByList expected1 ["u", "name"] `shouldBe` "test"

        it "get exnetds flag value" $
            getArgsByNameExtend expected1 "f" `shouldBe` "value4"

        it "get exnetds name from list" $
            getArgsByListExtend expected1 ["f"] `shouldBe` "value4"

        it "not read multi flags" $
            readArgs ["-p", "-ab", "--ab"] `shouldBe` [Args "p" "", Args "index1" "-ab", Args "ab" ""]

    -- MMV.Config

    describe "MMV.Config" $ do

        it "readConfigToList" $
            readConfigToList config `shouldBe` expected2

        it "readConfigToListMult" $
            readConfigToListMult config `shouldBe` expected3

        it "writeListToConfig" $
            readConfigToList (writeListToConfig config2)
                `shouldBe` config2

        it "writeListToConfigMult" $
            readConfigToListMult (writeListToConfigMult config3)
                `shouldBe` config3

        it "setConfigByName" $
            setConfigByName [("name2", "value2")] "name1" "value1"
                `shouldBe` [("name2", "value2"), ("name1", "value1")]

    -- MMV.Convert

    describe "MMV.Convert" $ do

        it "floatToString" $ floatToString 1234.1234 `shouldBe` "1234.1234"
        it "doubleToString" $ doubleToString 12345678.1234 `shouldBe` "12345678.1234"
